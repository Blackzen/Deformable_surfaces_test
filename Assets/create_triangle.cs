﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class create_triangle : MonoBehaviour {
    private List<Vector3> vertices;
    private List<int> triangles;
    private List<Vector2> uv;

    private Mesh mesh;
    private List<GameObject> handles;

    public GameObject control_point_prefab;
    
    private void Start()
    {
        Create_surface();
        Create_control_points();
    }

    void Update () {
        StartCoroutine("update_handles");
    }

    private IEnumerator update_handles() {
        int i = 0;
        while (i < vertices.Count)
        {
            //Debug.Log(i);
            vertices[i] = handles[i].transform.position - this.transform.position;
            i++;
        }
        mesh.vertices = vertices.ToArray();
        mesh.RecalculateBounds();      
        yield return null;
    }

    public void Create_surface()
    {
        vertices = new List<Vector3>();
        triangles = new List<int>();
        uv = new List<Vector2>();

        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "triangle";

        //assinging vertex position.
        Vector3 a, b, c;
        a = new Vector3(0, 0, 0);
        c = new Vector3(1, 0, 0);
        b = new Vector3(c.x / 2, 0, Mathf.Sqrt(Mathf.Pow(1,2) - Mathf.Pow(0.5f, 2)));

        vertices.Add(a);
        vertices.Add(b);
        vertices.Add(c);

        //setting order for triangle creation.
        triangles.Add(0);
        triangles.Add(1);
        triangles.Add(2);

        //attempt to create both sides of the mesh without duplicating vertices (shows black on both sides HELP!!!).
        //triangles.Add(2);
        //triangles.Add(1);
        //triangles.Add(0);

        //setting uv coordinates for surface for adding texture.
        uv.Add(new Vector2(0, 0));
        uv.Add(new Vector2(0.5f, 1));
        uv.Add(new Vector2(1, 0));

        //passing created information to mesh filter. 
        GetComponent<MeshFilter>().sharedMesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv.ToArray();

        //recalculating normals for proper ilumination.
        mesh.RecalculateNormals();
    }

    private void Create_control_points()
    {
        handles = new List<GameObject>();
        for (int i = 0; i < vertices.Count; i++)
        {
            GameObject handle = Instantiate(control_point_prefab, transform.position + vertices[i], Quaternion.identity) as GameObject;
            handle.transform.parent = this.transform;
            handles.Add(handle); 
        }
    }


    //used for drawing non interactable gizmos for intial vertex location.
    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        if (vertices == null)
        {
            return;
        }
        else{
            for (int i = 0; i < vertices.Count; i++)
            {
                Gizmos.DrawSphere(vertices[i], 0.01f);
            }
        }

	}*/

}
