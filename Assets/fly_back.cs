﻿using UnityEngine;
using System.Collections;

public class fly_back : MonoBehaviour {

    private Vector3 start_pos;

    Quaternion originalRotation;

    float rotateSpeed = 0.1f;
    float posSpeed = 0.000000001f;
    private Rigidbody rb;

    public float force_amount;

    public bool go_back = false;


    float current = 0;

    // Use this for initialization
    void Awake() {
        start_pos = transform.position;
        rb = GetComponent<Rigidbody>();
        originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update() {
        if (go_back)
        {         
            StartCoroutine("return_to_old_pos");
        }
        else {
            
            //StopCoroutine("return_to_old_pos");
        }

    }

    public IEnumerator return_to_old_pos() {

        float Distancia_total = Vector3.Distance(start_pos ,transform.position);

        //REVISAR ROTACION 
        current += 0.005f; 
        transform.rotation = Quaternion.Lerp(transform.rotation, originalRotation, current);
        
        transform.position = Vector3.Lerp(start_pos, transform.position, (Distancia_total*0.9f) /Distancia_total);
        

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        if (Distancia_total < 0.001f) {
            go_back = false;
            current = 0;
        }

        yield return null;
        //

        //go_back = true;
    }


    
}
