﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset_surface_points : MonoBehaviour {
    public GameObject surface;

    public bool come_back = false;


    void Update()
    {
        StartCoroutine("add_force");
    }


    public void change_state() {
        come_back = !come_back;
    }

    public IEnumerator add_force()
    {
            foreach (fly_back obj in transform.GetComponentsInChildren<fly_back>())
            {
                if (come_back)
                {
                    obj.go_back = true;
                }
                else
                {
                    obj.go_back = false;
                }


            }
            yield return null;
    }
    

}
