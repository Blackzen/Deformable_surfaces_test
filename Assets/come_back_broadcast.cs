﻿using UnityEngine;
using System.Collections;

public class come_back_broadcast : MonoBehaviour {
    public bool come_back = false;
	// Use this for initialization
	void Start () {
        //StartCoroutine("add_force");
    }

    // Update is called once per frame
    void Update() {

            
      
    }

    IEnumerator add_force()
    {
        foreach (fly_back obj in transform.GetComponentsInChildren<fly_back>())
        {
            if (come_back)
            {
                obj.force_amount = 5;
            }else {
                obj.force_amount = 0;
            }
            

        }
        yield return null;
    }

    public void change_state() {
        //come_back = !come_back;
        foreach (fly_back obj in transform.GetComponentsInChildren<fly_back>())
        {
            obj.go_back = true;

        }
    }

}
