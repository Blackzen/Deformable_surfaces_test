﻿using UnityEngine;
using System.Collections;

public class fly_back_2 : MonoBehaviour {

    public Transform _transform;
    public Vector3 posicionInicial;

    private float distancia;


    public bool activate = false;

    public float recorrido = 0;
    // Use this for initialization
    void Start () {
	    posicionInicial = _transform.position;
        
    }
	
	// Update is called once per frame
	void Update () {
        if (activate)
            {
            StartCoroutine("go_back");
            }
        }

    IEnumerator go_back() {
        if (Vector3.Distance(posicionInicial, transform.position) > 0.01f)
        {
            recorrido += 0.001f;
            _transform.position = Vector3.Lerp(transform.position, posicionInicial, recorrido);
            yield return new WaitForSeconds(0.1f);
        }
        else {
            
            transform.position = posicionInicial;
            recorrido = 0;
            activate = false;
            GetComponent<Reset_surface_points>().come_back = false;
        }
        
        //activate = false;
    }

}








