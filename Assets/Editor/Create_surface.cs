﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(create_triangle))]
public class Create_surface : Editor {
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        create_triangle myTarget = (create_triangle)target;
        if (GUILayout.Button("Build Triangle"))
        {
            myTarget.Create_surface();
        }
    }

}
